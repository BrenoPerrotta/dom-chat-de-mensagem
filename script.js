let escopo = document.querySelector(".box_final");
let textarea = document.querySelector(".box_mensages");

// Função para enviar mensagem
function enviar_mensagem() {
    let mensagem = textarea.value;
    escopo.append(mensagem + "\n");
    textarea.value = "";

}

// função para limpar mensagem
function limpar_mensagem() {
    escopo.innerHTML = "";

}

// Funções para ativar edição das mensagens já enviadas
// Vai descer o que já foi preenchido, habilitar o botão de salvar e desabilitar os demais botões
function ativar_edicao() {
    textarea.value = escopo.value

    document.querySelector(".button_editar").style.display = "none";
    document.querySelector(".button_excluir").style.display = "none";
    document.querySelector(".button_enviar").style.display = "none";
    document.querySelector(".modo_edicao_button_salvar").style.display = "block";

}

// Vai subir os ajustes, desabilitar o botão de salvar e habilitar os demais botões
function salvar_edicao() {
    var texto_editado = textarea.value;
    escopo.textContent = texto_editado;
    textarea.value = "";

    document.querySelector(".modo_edicao_button_salvar").style.display = "none";
    document.querySelector(".button_enviar").style.display = "block";
    document.querySelector(".button_editar").style.display = "block";
    document.querySelector(".button_excluir").style.display = "block";
    
}

// funções dos botões
let button_enviar = document.querySelector(".button_enviar");
button_enviar.addEventListener("click", enviar_mensagem);

let button_excluir = document.querySelector(".button_excluir");
button_excluir.addEventListener("click", limpar_mensagem);

let button_editar = document.querySelector(".button_editar");
button_editar.addEventListener("click", ativar_edicao);

let button_salvar = document.querySelector(".modo_edicao_button_salvar");
button_salvar.addEventListener("click", salvar_edicao);



